const client = require("../index");
require("dotenv").config();
const { glob } = require("glob");
const { promisify } = require("util");
const globPromise = promisify(glob);

client.on("interactionCreate", async (interaction) => {
  // Slash Commands
  const slashCommands = await globPromise(`${process.cwd()}/commands/*/*.js`);
  const arrayOfSlashCommands = [];

  // Map the slash commands into data to be processed
  slashCommands.map((value) => {
    const file = require(value);
    const splitted = value.split("/");
    const directory = splitted[splitted.length - 2];

    if (!file?.name) return;

    const properties = { directory, ...file };
    client.slashCommands.set(file.name, properties);

    if (["MESSAGE", "USER"].includes(file.type)) delete file.description;

    // Push the data
    const JSONCommand = {
      ...file,
      integration_types: [0, 1], // 0 for guild, 1 for user
      contexts: [0, 1, 2], // 0 for guild, 1 for app DMs, 2 for GDMs and other DMs
    };
    arrayOfSlashCommands.push(JSONCommand);
  });

  // Slash Command Handling
  if (interaction.isChatInputCommand()) {
    let commandData = [];

    // Filter to find the command for this interaction
    await arrayOfSlashCommands.forEach(command => {
      if (command.name === interaction.commandName) {
        commandData.push(command);
      }
    });

    // Process and parse the command data
    const parsedData = commandData[0];

    // Defer reply based on privacy settings
    if (interaction.commandName === "modal-example") {
      console.log("Modal - Skipping defer");
    } else {
      const isPrivate = parsedData?.private;
      await interaction.deferReply({
        ephemeral: !!isPrivate,
      }).catch(() => {});
    }

    const cmd = client.slashCommands.get(interaction.commandName);
    if (!cmd) {
      return interaction.followUp({ content: "An error has occurred" });
    }

    const args = [];
    for (let option of interaction.options.data) {
      if (option.type === "SUB_COMMAND") {
        if (option.name) args.push(option.name);
        option.options?.forEach((x) => {
          if (x.value) args.push(x.value);
        });
      } else if (option.value) args.push(option.value);
    }

    // Check if the interaction is in a guild and assign member accordingly
    if (interaction.inGuild()) {
      interaction.member = interaction.guild.members.cache.get(interaction.user.id);
    }

    // Run the command
    cmd.run(client, interaction, args);
  }

  // Context Menu Handling
  if (interaction.isContextMenuCommand()) {
    await interaction.deferReply({ ephemeral: false });
    const command = client.slashCommands.get(interaction.commandName);
    if (command) command.run(client, interaction);
  }
});
