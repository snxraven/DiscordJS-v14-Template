const { EmbedBuilder } = require('discord.js');
const { ActionRowBuilder, ModalBuilder, TextInputBuilder, TextInputStyle } = require('discord.js');

module.exports = {
    name: "modal-example",
    description: "Show a demo modal!",

    run: async (client, interaction) => {

        // Declare a random ID for the modal to ensure unique interactions
        const modalId = `modal-${Math.floor(Math.random() * 99999999999999).toString()}`;

        // Check if this is a chatInput command interaction
        if (!interaction.isChatInputCommand()) return;

        // Create the modal
        const modal = new ModalBuilder()
            .setCustomId(modalId)
            .setTitle('This is an example modal');

        // Create a text input component for the modal
        const modalInputData = new TextInputBuilder()
            .setCustomId('modalInput')
            .setLabel("What text do you want to send?")
            .setStyle(TextInputStyle.Paragraph); // Allows for multi-line text

        // Wrap the text input in an action row
        const modalInputRow = new ActionRowBuilder().addComponents(modalInputData);

        // Add the input row to the modal
        modal.addComponents(modalInputRow);

        // Show the modal to the user
        await interaction.showModal(modal);

        // Handle the modal submission within the same interaction listener
        client.once('interactionCreate', async (modalInteraction) => {

            // Ensure we are handling the correct modal submission
            if (!modalInteraction.isModalSubmit() || modalInteraction.customId !== modalId) return;

            // Retrieve the data entered by the user
            const modalInputDataString = modalInteraction.fields.getTextInputValue('modalInput');

            // Create an embed to display the user input
            const embed = new EmbedBuilder()
                .setColor("#FF0000")
                .setTitle("Your input!")
                .setDescription(`You said: ${modalInputDataString}`)
                .setTimestamp()
                .setFooter({ text: `Requested by ${modalInteraction.user.tag}`, iconURL: modalInteraction.user.displayAvatarURL() });

            // Reply to the modal submission with the embed
            await modalInteraction.reply({ embeds: [embed] });
        });
    }
};
