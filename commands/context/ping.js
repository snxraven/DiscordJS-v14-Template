const { EmbedBuilder } = require('discord.js');
const {ApplicationCommandType } = require('discord.js');

module.exports = {
  name: "ping-test",
  type: ApplicationCommandType.Message,
  run: async (client, interaction) => {
    const embed = new EmbedBuilder()
      .setColor("#FF0000")
      .setTitle("🏓 Pong!")
      .setDescription(`Latency : ${client.ws.ping}ms`)
      .setTimestamp()
      .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: `${interaction.user.displayAvatarURL()}` });
    interaction.followUp({ embeds: [embed] });
  },
};
