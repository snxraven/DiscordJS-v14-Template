# Discord Bot with Slash Commands and Modals

This project is a fully-featured Discord bot written in Node.js, using the `discord.js` library. It includes functionalities like handling slash commands, context menu commands, and displaying modals for user input.

## Features

- **Slash Commands**: Commands that can be triggered using `/` in Discord, like `/ping` to check bot latency.
- **Modals**: Interactive modals where users can input data, for example, `/modal-example`.
- **Context Menu Commands**: Right-click context menu commands for users or messages.
- **Ephemeral Replies**: Ability to send private, ephemeral replies to users.
- **Dynamic Command Registration**: Automatically registers commands based on the project directory structure.
- **User Installed App Support**: Automatically registers commands global to Discord via User Apps

## Installation

### Prerequisites

Ensure you have the following installed:

- [Node.js](https://nodejs.org/) (v16.6.0 or higher)
- [Discord.js](https://discord.js.org/) (v14.x)
- A Discord Bot Token (see [here](https://discord.com/developers/docs/getting-started#configuring-your-bot) for instructions)

### Clone the Repository

```bash
git clone https://git.ssh.surf/snxraven/DiscordJS-v14-Template.git
cd DiscordJS-v14-Template
```

### Install Dependencies

```bash
npm install
```

### Environment Variables

Create a `.env` file in the root of your project directory with the following content:

```
TOKEN=YOUR_DISCORD_BOT_TOKEN
DISCORD_CLIENT_ID=APPIDHERE
```

Replace `YOUR_DISCORD_BOT_TOKEN` with your actual bot token from the Discord Developer Portal.

## File Structure

- `index.js`: The entry point of the bot that initializes the client and loads commands and events.
- `handler/index.js`: Dynamically loads commands and events and registers slash commands with Discord.
- `events/`: Folder containing event listeners such as `ready.js` (bot ready event) and `interactionCreate.js` (command handling).
- `commands/`: Folder containing command files. Commands are organized into subfolders based on category.
  - `commands/info/`: Contains commands like `/ping` and `/modal-example`.
  - `commands/context/`: Contains context menu commands like `ping-test`.
  
### Commands

- `ping`: Returns the bot's websocket latency with an ephemeral reply.
- `modal-example`: Displays a modal for user input.
- `ping-test`: A context menu command that returns latency when right-clicking a message.

## Running the Bot

To start the bot, run:

```bash
node index.js
```

If everything is set up correctly, you should see the following message:

```
YourBotName is up and ready to go!
```

The bot will automatically register slash commands for every guild it's in.

## Command Examples

### Slash Command: `/ping`

Returns the bot's websocket latency in an embed message.

### Slash Command: `/modal-example`

Opens a modal where users can input text, which is then displayed back to them.

### Context Menu Command: `ping-test`

Available by right-clicking a message and choosing this context command. It shows the bot's latency.

## Adding New Commands

To add a new command:

1. Create a new `.js` file in the `commands/` folder under the appropriate subfolder.
2. Define your command with the structure used in the existing commands.
3. Restart the bot to automatically load the new command.

Example command structure:

```js
module.exports = {
  name: "new-command",
  description: "Describe your command here",
  run: async (client, interaction) => {
    // Command logic
  },
};
```

## Modals

The bot supports modals for user input. To add a new modal, create a new command in the `commands/` folder and use the `ModalBuilder` from `discord.js` to display a modal.

Example modal code:

```js
const { ModalBuilder, TextInputBuilder, TextInputStyle } = require('discord.js');
const { ActionRowBuilder } = require('discord.js');

module.exports = {
    name: "modal-example",
    description: "Show a demo modal!",
    run: async (client, interaction) => {
        const modal = new ModalBuilder()
            .setCustomId('example-modal')
            .setTitle('Example Modal');
        
        const input = new TextInputBuilder()
            .setCustomId('input-field')
            .setLabel("Your Input")
            .setStyle(TextInputStyle.Paragraph);

        const row = new ActionRowBuilder().addComponents(input);
        modal.addComponents(row);

        await interaction.showModal(modal);
    }
};
```

## Handling Ephemeral Replies

Commands can return ephemeral (private) responses, making replies visible only to the command invoker. To enable ephemeral replies, add a `private` property to the command definition:

```js
module.exports = {
  name: "ping",
  description: "Returns latency",
  private: true, // This makes the reply private
  run: async (client, interaction) => {
    // Command logic here
  },
};
```

## Events

The bot listens for two primary events:

- `ready`: Triggered when the bot is logged in and ready.
- `interactionCreate`: Triggered when a user interacts with the bot through slash commands, modals, or context menu commands.

## Troubleshooting

- Ensure you have the correct bot token in your `.env` file.
- Make sure your bot has the necessary permissions to register commands in the guilds it's in.
- If commands aren’t registering, try manually clearing the commands in the Discord Developer Portal or use `guild-specific` commands to speed up testing.
