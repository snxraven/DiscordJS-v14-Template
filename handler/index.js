require("dotenv").config();
const { glob } = require("glob");
const { promisify } = require("util");
const globPromise = promisify(glob);
const { REST } = require('@discordjs/rest');
const Discord = require('discord.js');

module.exports = async (client) => {
  const rest = new REST({ version: '10' }).setToken(process.env.TOKEN);

  // Slash Commands
  const slashCommands = await globPromise(`${process.cwd()}/commands/*/*.js`);
  const arrayOfSlashCommands = [];
  
  slashCommands.map((value) => {
    const file = require(value);
    const splitted = value.split("/");
    const directory = splitted[splitted.length - 2];

    if (!file?.name) return;

    const properties = { directory, ...file };
    client.slashCommands.set(file.name, properties);

    if (["MESSAGE", "USER"].includes(file.type)) delete file.description;

    // Add integration types and contexts to support user apps
    const JSONCommand = {
      ...file,
      integration_types: [0, 1], // 0 for guild, 1 for user
      contexts: [0, 1, 2], // 0 for guild, 1 for DMs, 2 for GDMs and other DMs
    };

    arrayOfSlashCommands.push(JSONCommand);
  });

  // Events
  const eventFiles = await globPromise(`${process.cwd()}/events/*.js`);
  eventFiles.map((value) => require(value));

  // Slash Commands Register
  client.on("ready", async () => {
    try {
      // Register for all the guilds the bot is in
      await rest.put(
        Discord.Routes.applicationCommands(client.user.id),
        { body: arrayOfSlashCommands }
      );
      console.log("Successfully registered application commands.");
    } catch (error) {
      console.error("Error while registering application commands:", error);
    }
  });
};
